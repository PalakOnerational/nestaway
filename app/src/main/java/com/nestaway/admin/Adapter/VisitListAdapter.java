package com.nestaway.admin.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nestaway.admin.Model.PendingList;
import com.nestaway.admin.R;

import org.json.JSONArray;
import org.json.JSONException;

public class VisitListAdapter extends RecyclerView.Adapter<VisitListAdapter.MyViewHolder> {

        private JSONArray data;
        private Context activity;
        private PendingList pending;



        public VisitListAdapter(Context context, JSONArray list) {
            this.activity = context;
            this.data = list;
            pending=new PendingList(list);
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView name, email, time,telephone,status;

            public MyViewHolder(View view) {
                super(view);
                name = (TextView) view.findViewById(R.id.name);
                email = (TextView) view.findViewById(R.id.email);
                time = (TextView) view.findViewById(R.id.time);
                telephone=(TextView)view.findViewById(R.id.phone);
                status=(TextView)view.findViewById(R.id.status);
            }
        }


        @Override
        public VisitListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.visit_item, parent, false);

            return new VisitListAdapter.MyViewHolder(itemView);

        }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        try {
            holder.name.setText(pending.getName(position));
            holder.telephone.setText(pending.getPhone(position));
            holder.email.setText(pending.getPhone(position));
            holder.status.setText(pending.getVisitStatus(position));
            holder.time.setText(pending.getdate(position)+" "+ pending.getTime(position));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return data.length();
    }
}
