package com.nestaway.admin.Util;

import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.nestaway.admin.Model.Pending;
import com.nestaway.admin.Model.PendingList;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.HashMap;

/**
 * Created by palaksachdeva on 05/03/18.
 */

public class ReadJsonFile {
    String json = "";
    public String ReadFile() {
        try {
            File yourFile = new File(Environment.getExternalStorageDirectory(), "/Download/generated (1).json");
//            FileInputStream stream = new FileInputStream(yourFile);
//            String jString = null;
//            try {
//                FileChannel fc = stream.getChannel();
//                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
//            /* Instead of using default, pass in a decoder. */
//                jString = Charset.defaultCharset().decode(bb).toString();
//                Toast.makeText(AppController.getAppContext(),jString, Toast.LENGTH_SHORT).show();
//            }
//            finally {
//                stream.close();
//            }
            BufferedReader reader = new BufferedReader(new FileReader(yourFile));

            try {
                StringBuilder sb = new StringBuilder();
                String line = reader.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("\n");
                    line = reader.readLine();
                }
                json = sb.toString();

            } finally {
                reader.close();
            }

            Pending jsonObj = new Pending(json);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return json;
    }

}
