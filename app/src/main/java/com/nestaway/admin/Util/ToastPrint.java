package com.nestaway.admin.Util;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

/**
 * Created by Admin on 10/13/2016.
 */
public class ToastPrint {
    private static ProgressDialog progressDialog;
    String toast;
    public ToastPrint(Context context){

        progressDialog=new ProgressDialog(context);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
      //  this.toast=toast;
     //   Toast.makeText(AppController.getAppContext(),toast,Toast.LENGTH_LONG).show();
    }
    public void printToast(String toast){

        Toast.makeText(AppController.getAppContext(),toast, Toast.LENGTH_SHORT).show();
    }
    public ProgressDialog dialogPrint(String message){

        progressDialog.setMessage(message);
        progressDialog.show();
            return progressDialog;


    }
    public  void dismiss(){
        progressDialog.dismiss();
    }
}
