package com.nestaway.admin.Util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Validator {

    public static boolean isName(String name) {
        return !checkEmpty(name) && name.length() > 1&& name.length() < 30;
    }
    public static boolean isPincode(String name) {
        return !checkEmpty(name) && name.length() >5;
    }

    public static boolean isPassword(String password) {
        return !checkEmpty(password) && password.length() >5;
    }

    public static boolean isEmpty(String name) {
        return !checkEmpty(name);
    }


    public static boolean isEmail(String email) {
        if (checkEmpty(email)) return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

    }

    public static boolean isPhone(String phone) {

        if (checkEmpty(phone)) return false;

        String PATTERN = "[0-9]{10}";
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    private static boolean checkEmpty(String text) {
        return TextUtils.isEmpty(text);
    }
}
