package com.nestaway.admin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.nestaway.admin.Network.Api;
import com.nestaway.admin.Network.ApiTO;
import com.nestaway.admin.R;
import com.nestaway.admin.Util.ReadJsonFile;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by palaksachdeva on 05/03/18.
 */

public class VisitDetailActivity extends AppCompatActivity {
    private TextView importText;
    private EditText name, email, telephone;
    private Spinner status, time;
    private TextView date;
    private ArrayList uniqueTimeArray;
    private ArrayList busySlotArray;
    private int repeat = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        init();
        Intent intent = getIntent();
        busySlotArray = new ArrayList<String>(intent.getStringArrayListExtra("timeArray"));
        uniqueTimeArray = new ArrayList();
        importText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReadJsonFile readJsonFile = new ReadJsonFile();
                readJsonFile.ReadFile();
            }
        });
        List<String> myArrayList = Arrays.asList(getResources().getStringArray(R.array.time_array));
        for (int i = 0; i < myArrayList.size(); i++) {
            for (int j = 0; j < busySlotArray.size(); j++) {
                if (busySlotArray.get(j).toString().equalsIgnoreCase(myArrayList.get(i))) {

                    repeat = 1;
                }
            }
            if (repeat == 0) {
                uniqueTimeArray.add(myArrayList.get(i));
            }
            repeat = 0;
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, uniqueTimeArray);
        time.setAdapter(adapter);
        ArrayAdapter statusAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.visit_status));
        status.setAdapter(statusAdapter);
        if(intent.getStringExtra("page").equalsIgnoreCase("update")){
            try {
                JSONObject jsonObject=new JSONObject(intent.getStringExtra("data"));
                name.setText(jsonObject.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void init() {
        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        telephone = (EditText) findViewById(R.id.phone);
        status = (Spinner) findViewById(R.id.status);
        time = (Spinner) findViewById(R.id.time);
        date = (TextView) findViewById(R.id.date);
        importText = (TextView) findViewById(R.id.import_text);
    }
}
