package com.nestaway.admin.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.nestaway.admin.Adapter.VisitListAdapter;
import com.nestaway.admin.Model.Pending;
import com.nestaway.admin.Network.Api;
import com.nestaway.admin.Network.ApiTO;
import com.nestaway.admin.Network.ApiUrl;
import com.nestaway.admin.R;
import com.nestaway.admin.Util.AdapterCommunicator;
import com.nestaway.admin.Util.AppController;
import com.nestaway.admin.Util.ReadJsonFile;
import com.nestaway.admin.Util.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static java.security.AccessController.getContext;

/**
 * Created by palaksachdeva on 04/03/18.
 */

public class VisitListActivity extends AppCompatActivity {
    private Api mApi;
    private RecyclerView pendings;
    private SwipeRefreshLayout swipeContainer;
    private ApiTO apiTO;
    private Activity activity;
    public String response;
    private VisitListAdapter visitListAdapter;
    String todayDate;
    private JSONArray slotArray;
    private ArrayList timeArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visits);
        init();
        activity = this;
        ReadJsonFile readJsonFile = new ReadJsonFile();
        response = readJsonFile.ReadFile();
        Log.i("response", response);
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy");
        todayDate = df.format(c);
        Log.i("date",todayDate);
        renderData(response);


        pendings.addOnItemTouchListener(
                new RecyclerItemClickListener(activity, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View v, int position) {

                        Intent visitDetails = new Intent(activity, VisitDetailActivity.class);
                        visitDetails.putExtra("timeArray", timeArray);
                        visitDetails.putExtra("page", "update");
                        try {
                            visitDetails.putExtra("data",slotArray.getJSONObject(position).toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        activity.startActivity(visitDetails);

                    }
                })
        );

    }

    private void init() {
        pendings = (RecyclerView) findViewById(R.id.recycler_view);


    }

    public void renderData(String response) {
        Pending pending = new Pending(response);
        slotArray = new JSONArray();
        timeArray=new ArrayList();
        try {
            for (int i = 0; i < pending.getSlotBooking().length(); i++) {
                Log.i("date",pending.getdate(i));
                if (pending.getdate(i).toString().equalsIgnoreCase(todayDate)) {
                    slotArray.put(pending.getVisitData(i));
                    timeArray.add(pending.getTime(i));
                }
            }
            visitListAdapter = new VisitListAdapter(activity, slotArray);
            LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
            pendings.setLayoutManager(layoutManager);
            pendings.setAdapter(visitListAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
