package com.nestaway.admin.Model;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 10/2/2016.
 */
public class Pending {
    String response;
    JSONObject responseObject, visitData, dataObject, visitObject;
   JSONArray visits;

    public Pending(String response) {
        this.response = response;
        try {
            responseObject = new JSONObject(response);
        } catch (JSONException e) {

            e.printStackTrace();
        }

    }

    public JSONArray getSlotBooking() throws JSONException {

        visits = responseObject.getJSONArray("slot_booking");
        return visits;
    }


    public JSONObject getVisitData(int position)throws JSONException {
        visitObject= visits.getJSONObject(position);
        return visitObject;
    }
    public String getName(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("name");
    }
    public String getEmail(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("email");
    }
    public String getPhone(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("phone");
    }
    public String getTime(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("time");
    }
    public String getdate(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("date");
    }
}
