package com.nestaway.admin.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 10/2/2016.
 */
public class PendingList{
    JSONArray dataArray;
    JSONObject visitObject;
    public PendingList(JSONArray list){
        dataArray=list;
    }
    public void getVisitData(int position)throws JSONException {
        visitObject= dataArray.getJSONObject(position);
    }
    public String getName(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("name");
    }
    public String getEmail(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("email");
    }
    public String getPhone(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("phone");
    }
    public String getTime(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("time");
    }
    public String getdate(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("date");
    }
    public String getVisitStatus(int position) throws JSONException {
        getVisitData(position);
        return visitObject.getString("visit_status");
    }

}
