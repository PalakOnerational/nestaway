package com.nestaway.admin.Network;

import android.content.pm.PackageManager;
import android.net.Uri;

import com.nestaway.admin.R;
import com.nestaway.admin.Util.AppController;


public class ApiUrl {

    private static String baseApiUrl;
    private static int versionCode;

    static {
        baseApiUrl ="fff";
    }

    public interface Protocol {
        String HTTP = "http://";
        String HTTPS = "https://";
    }

    private interface Version {
        String V1 = "sale/visit";
        String V2 = "/v2";
    }

    public static String DEFAULT_PROTOCOL = Protocol.HTTP;

    private static final String DEFAULT_VERSION = Version.V1;

    public static String getBaseUrl() {
        return DEFAULT_PROTOCOL + baseApiUrl;
    }

    private interface PATH {
        String COMMON = "common";
        String LOGIN = "login";
        String API = "api";
        String SIGN_OUT = "sign_out";
        String HOMES = "homes";
        String LOGOUT = "logout";
        String FORGET="forgotten";
    }

    public interface PARAM {
        String ROUTE = "route";
        String TOKEN = "token";
        String API = "api";
        String VERSIONCODE = "version_code";
        String VISITID = "visit_id";
        String FILTERSTATUS = "filter_status";
        String FILTERCANCEL = "filter_cancel";
        String FILTERDATE = "filter_date_visit";
        String VISITSTATUS = "visit_status";
        String VISETREASON = "visit_status_reason";
        String EMPLOYEEID = "employee_id";
        String VISITSTATUSID = "visit_status_id";
    }

    private static void getToken() {
        try {
            versionCode = AppController.getAppContext().getPackageManager().getPackageInfo(AppController.getAppContext().getPackageName(), 0).versionCode;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String login() {
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "common");
        builder.appendPath(PATH.LOGIN);
        builder.appendPath(PATH.API);
        return Uri.decode(builder.build());
    }
    public static String forget() {
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, PATH.API);
        builder.appendPath(PATH.FORGET);
        return Uri.decode(builder.build());
    }

    public static String logout(String token) {
        UrlBuilder builder = new UrlBuilder();
        //builder.append(PATH.COMMON);
        builder.appendQueryParams(PARAM.ROUTE, "api");
        builder.appendPath(PATH.LOGOUT);
        builder.appendQueryParams(PARAM.TOKEN, token);
        return Uri.decode(builder.build());
    }

    public static String visitList(String token, String status, String date) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, String.valueOf("api/visit").toString());
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.FILTERSTATUS, status);
        builder.appendQueryParams(PARAM.FILTERDATE, date);
        return Uri.decode(builder.build());
    }



    public static String visitListDetail(String token, String visitId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, String.valueOf("api/visit/info").toString());
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        return Uri.decode(builder.build());
    }

    public static String followStatus(String token, String visitId, String visitStatus, String reason) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, String.valueOf("api/visit/followUpVisit").toString());
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        builder.appendQueryParams(PARAM.VISITSTATUS, visitStatus);
        builder.appendQueryParams(PARAM.VISETREASON, reason);
        return Uri.decode(builder.build());
    }
    public static String CancelStatus(String token, String visitId, String visitStatus, String reason) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, String.valueOf("api/visit/cancelVisit").toString());
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        builder.appendQueryParams(PARAM.VISITSTATUS, visitStatus);
        builder.appendQueryParams(PARAM.VISETREASON, reason);
        return Uri.decode(builder.build());
    }


    public static String cancelReason(String token, String visitId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/getVisitStatusReasons");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VISITSTATUS, visitId);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        return Uri.decode(builder.build());
    }

    public static String updateEmployee(String token, String visitId, String employeeId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/updateEmployee");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        builder.appendQueryParams(PARAM.EMPLOYEEID, employeeId);
        return Uri.decode(builder.build());
    }

    public static String ConvertVisit(String token, String visitId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/convertVisit");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        return Uri.decode(builder.build());
    }
    public static String sendPayment(String token, String visitId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/sendPaymentLink");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        return Uri.decode(builder.build());
    }

    public static String rescheduleVisit(String token, String visitId) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/rescheduleVisit");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        builder.appendQueryParams(PARAM.VISITID, visitId);
        return Uri.decode(builder.build());
    }

    public static String EmployeeList(String token) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "api/visit/getSalesEmployees");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        return Uri.decode(builder.build());
    }

    public static String FcmToken(String token) {
        getToken();
        UrlBuilder builder = new UrlBuilder();
        builder.appendQueryParams(PARAM.ROUTE, "user/user/addFCMToken");
        builder.appendQueryParams(PARAM.TOKEN, token);
        builder.appendQueryParams(PARAM.VERSIONCODE, versionCode);
        return Uri.decode(builder.build());
    }
}
