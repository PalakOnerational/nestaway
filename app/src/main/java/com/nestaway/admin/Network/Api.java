package com.nestaway.admin.Network;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.nestaway.admin.Util.AppController;
import com.nestaway.admin.Util.ToastPrint;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class Api {

    JSONObject responseVolley;
    String errorVolley;
    String response;
    private ToastPrint toastPrint;
    public static final int DEFAULT_TIMEOUT_MS = 5000;

    /** The default number of retries */
    public static final int DEFAULT_MAX_RETRIES = 0;
    private ProgressDialog progressDialog;
    /** The default backoff multiplier */
    public static final float DEFAULT_BACKOFF_MULT = 1f;

    public String callLoginApi(final ApiTO apiTO) {
        toastPrint=new ToastPrint(AppController.getAppContext());
// call api for login using volley to obtain JSONObject
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(apiTO.getMethod(),
                apiTO.getUrl(), apiTO.getRequest()
                ,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        responseVolley = response;
                        String fragment = apiTO.getActivity();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
               toastPrint.dismiss();
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    toastPrint.printToast(message);
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    toastPrint.printToast(message);
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    toastPrint.printToast(message);
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    toastPrint.printToast(message);
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    toastPrint.printToast(message);
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    toastPrint.printToast(message);
                }

            }
        });

        AppController.getInstance().addToRequestQueue(jsonObjReq);


        if (responseVolley != null) {
            response = responseVolley.toString();
        } else {
            response = String.valueOf(errorVolley);
        }

        return response;
    }



}
