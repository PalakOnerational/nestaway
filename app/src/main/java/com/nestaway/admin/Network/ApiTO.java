package com.nestaway.admin.Network;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Admin on 9/30/2016.
 */
public class ApiTO {
    String url;
    JSONObject request;
    JSONArray requestArray;
    int method;
    String type="";
    String activity;
    String fragment;

    public String getFragment() {
        return fragment;
    }

    public void setFragment(String fragment) {
        this.fragment = fragment;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public JSONObject getRequest() {
        return request;
    }

    public void setRequest(JSONObject request) {
        this.request = request;
    }
    public void setRequestArray(JSONArray request) {
        this.requestArray = request;
    }
    public JSONArray getRequestArray() {
        return requestArray;
    }
    public int getMethod() {
        return method;
    }

    public void setMethod(int method) {
        this.method = method;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
