package com.nestaway.admin.Network;

import android.net.Uri;

import com.nestaway.admin.Util.StringUtil;


public class UrlBuilder {

    private StringBuilder mBuilder;
    private boolean mFirstQueryFieldInserted = false;
    public static final String UTF_8 = "UTF-8";

    public UrlBuilder() {
        mBuilder = new StringBuilder();
        setBaseUrl();
    }

    public UrlBuilder(String baseUrl) {
        if (baseUrl.contains("?")) {
            mFirstQueryFieldInserted = true; // Already having a query field
        }
        mBuilder = new StringBuilder(baseUrl);
    }

    private UrlBuilder setBaseUrl() {
        mBuilder.append(ApiUrl.getBaseUrl());
        return this;
    }

    public UrlBuilder appendPath(String path) {
        mBuilder.append(StringUtil.FORWARD_SLASH)
                .append(path);

        return this;
    }

    public UrlBuilder appendPath(int id) {
        return appendPath(String.valueOf(id));
    }

    public UrlBuilder appendPath(long id) {
        return appendPath(String.valueOf(id));
    }

    public UrlBuilder appendQueryParams(String key, String[] params) {

        appendQueryMark();
        mBuilder.append(key)
                .append(StringUtil.EQUAL);

        for (String param : params) {
            mBuilder.append(Uri.encode(param, UTF_8))
                    .append(",");
        }

        mBuilder.deleteCharAt(mBuilder.length() - 1); // remove last comma

        return this;

    }

    public UrlBuilder appendQueryParams(String key, String value) {
        appendQueryMark();
        mBuilder.append(key)
                .append(StringUtil.EQUAL)
                .append(Uri.encode(value, UTF_8));

        return this;
    }

    public UrlBuilder appendQueryParams(String key, int value) {
        return appendQueryParams(key, String.valueOf(value));
    }

    public UrlBuilder appendQueryParams(String key, double value) {
        return appendQueryParams(key, String.valueOf(value));
    }

    public UrlBuilder appendQueryParams(String key, boolean value) {
        return appendQueryParams(key, String.valueOf(value));
    }

    public UrlBuilder appendQueryParams(UrlBuilder params) {
        mBuilder.append(params.build());
        return this;
    }

    public String build() {
        return Uri.parse(mBuilder.toString()).toString();
    }

    private UrlBuilder appendQueryMark() {
        if (mFirstQueryFieldInserted) {
            mBuilder.append(StringUtil.AMPERSAND);
        } else {
            mBuilder.append(StringUtil.QUESTION_MARK);
            mFirstQueryFieldInserted = true;
        }

        return this;
    }

    public UrlBuilder(boolean firstQueryFiledInserted) {
        mBuilder = new StringBuilder();
        mFirstQueryFieldInserted = firstQueryFiledInserted;
    }
}
